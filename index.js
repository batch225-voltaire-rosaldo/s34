//Activity S34
const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// GET Home +==============================
app.get("/home", (req, res) => {
	res.send("Welcome to the homepage!");
});

//POST ==================================
let users = [];

app.post("/signup", (req, res) => {

	console.log(req.body);

	//
	if(req.body.userName !== ''){
		users.push(req.body);
		res.send(`User ${req.body.userName} successfully added`);
	} else {
		res.send("Please input username.");
	}
});

// DELETE User =============================
app.delete('/delete-user', (req, res) => {

	let message;

	for(let i = 0; i < users.length; i ++ ){

		if(req.body.userName == users[i].userName){

			users.pop(req.body);
			message = `User has been deleted.`;

			break;
			
		} else {

			message = "User does not exist.";
		};
	};
	res.send(message);
});


// Display User ===============================
app.get('/users', (req, res) => {
	res.json(users);
});

// Listen =====================================
app.listen(port, () => console.log(`Server running at port ${port}`));



























